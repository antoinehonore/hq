from sklearn.metrics import mean_squared_error, accuracy_score,\
                            roc_auc_score, precision_score,\
                            recall_score, average_precision_score, f1_score, confusion_matrix
from parse import parse
import numpy as np
from functools import partial
from hq.src.target_utils import to_onehot
from hq.src.utils import githash
import sys

eval_metrics = {"MSE": lambda y_true, y_hat: mean_squared_error(y_true, y_hat),
                "Acc": lambda y_true, y_hat: accuracy_score(y_true, cont_to_binary(y_hat, n=y_true.shape[1])),
                "spec": lambda y_true, y_hat: spec_score(y_true, cont_to_binary(y_hat, n=y_true.shape[1])),
                "r-sen": lambda y_true, y_hat: recall_score(y_true, cont_to_binary(y_hat, n=y_true.shape[1]), average='macro'),
                "AUROC": lambda y_true, y_hat: roc_auc_score(y_true, y_hat),
                "avgPr": lambda y_true, y_hat: partial(average_precision_score, average="macro")(y_true, y_hat),
                "f1-score": lambda y_true, y_hat: f1_score(y_true, cont_to_binary(y_hat, n=y_true.shape[1]), average="macro"),
                "p-ppv": lambda y_true, y_hat: precision_score(y_true, cont_to_binary(y_hat, n=y_true.shape[1]), average="macro"),
                "npv": lambda y_true, y_hat: npv_score(y_true, cont_to_binary(y_hat, n=y_true.shape[1])),
                }


def conf_mat(y_true, y_pred):
    """Conf mat on (n_samples,n_classes) probability matrices."""
    return confusion_matrix(np.argmax(y_true, axis=1), np.argmax(y_pred, axis=1)).ravel()


def npv_score(y_true, y_pred):
    tn, fp, fn, tp = conf_mat(y_true, y_pred)
    return tn / (tn + fn)


def spec_score(y_true, y_pred):
    tn, fp, fn, tp = conf_mat(y_true, y_pred)
    return tn / (tn + fp)


def cont_to_binary(y, n=None):
    return to_onehot(np.argmax(y, axis=1), n=n)


def test_cont_to_binary():
    y = np.array([[-1, -2], [-1000, -999], [0.3, 0.7]])
    assert ((cont_to_binary(y) == np.array([[1, 0], [0, 1], [0, 1]])).all())


def test_format_score():
    assert(format_score("abc", 1.23456789) == "abc:1.23457")


def format_score(k, s):
    return "{}:{}".format(k, round(s, 5))


def test_parse_score():
    assert(parse_score("abc:1.23457") == ["abc", 1.23457])


def parse_score(s_str):
    return list(parse("{}:{:f}", s_str))


def format_eval_line(out_mdl, tr_eval_str, te_eval_str, val_score=None):
    if val_score is None:
        str_ = "{} train\t{} test\t{}".format(out_mdl, " ".join(tr_eval_str), " ".join(te_eval_str))
    else:
        val_line = " ".join([format_score(k,v) for k,v in val_score.items()])
        str_ = "{}\tval {}\ttrain {}\ttest {}".format(out_mdl, val_line, " ".join(tr_eval_str), " ".join(te_eval_str))
    return str_


def parse_eval_line(fname):
    with open(fname, "r") as f:
        o = f.read().strip().split("\n")[0]

    _, algo, feat_mode, tr_eval_str, te_eval_str = parse("{}/models/{}.{}.mdl train\t{} test\t{}", o)

    return [algo, feat_mode, [parse_score(s_str) for s_str in tr_eval_str.split(" ")],
                             [parse_score(s_str) for s_str in te_eval_str.split(" ")]]


def write_eval_line(mdl, out_mdl, out_results, eval_metrics=eval_metrics):
    # Evaluate
    te_eval_str = [format_score(k, fun(mdl.userdata["ttest"], mdl.userdata["xtest_hat"]))
                   for k, fun in eval_metrics.items()]

    tr_eval_str = [format_score(k, fun(mdl.userdata["ttrain"], mdl.userdata["xtrain_hat"]))
                   for k, fun in eval_metrics.items()]

    if "fit_time" in mdl.userdata.keys():
        tr_eval_str = tr_eval_str  + [format_score("fit_time", mdl.userdata["fit_time"])]

    eval_line = " ".join([githash(), format_eval_line(out_mdl,
                                                      tr_eval_str,
                                                      te_eval_str,
                                                      val_score=mdl.userdata["val_score"])])

    if out_results != "-":
        with open(out_results, "a") as f:
            print(eval_line, file=f)
    else:
        print(eval_line, file=sys.stdout)

    return eval_line


if __name__ == "__main__":
    # test_cont_to_binary()
    pass
